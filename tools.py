# Create screen shots tools.
#
import os
import logging


# Create subdir for screen shots.
#
def assure_path_exists(path):
    logging.debug('check exists dir: ' + path)
    sdir = os.path.dirname(path)
    if not os.path.exists(sdir):
        logging.debug('creating dir: ' + path)
        os.makedirs(sdir)
        logging.debug('dir created.')

# Class for creating screen shots
#
class ScreenShot:

    def __init__(self, driver, namepattern, dirName):
        self.screenShotIndex = 1
        self.driver = driver
        self.namepattern = namepattern
        self.dirName = dirName
        self.disabled = False

    def create(self):
        if not self.disabled:
            assure_path_exists(self.dirName)
            filename = "{}{}{}".format(os.path.join(self.dirName, self.namepattern), self.screenShotIndex, '.png')
            saved = self.driver.save_screenshot(filename)
            logging.debug('screen shot %i [%s] saving result: %r', self.screenShotIndex, filename, saved)
            self.screenShotIndex += 1

    def disable(self):
        self.disabled = True

    def enable(self):
        self.disabled = False