# This file contains setting for Games app
#

LOG_FILENAME = 'games.log'

PYTHON_EXECUTE_PATH = 'C:/Python27/node_modules/phantomjs/lib/phantom/phantomjs.exe'

TARGET_MOVEMENT_PATTERN_URL = 'http://games.espn.go.com/flb/clubhouse?leagueId={0}&teamId={1}&seasonId=2014'


# Screen shot name and dir
#
SCREEN_SHOT_DIRECTORY = './screenshots/'
SCREEN_SHOT_FILENAME_PREFIX = 'screenshot_clubhouse_'

# Sign in URL for SignInActions
#
AJAX_SIGNING_ACTION_URL = 'http://games.espn.go.com/flb/clubhouse?leagueId=25748&teamId=10&seasonId=2014'
SIMPLE_SIGNIN_ACTION_URL = 'http://games.espn.go.com/flb/leaguesetup/ownerinfo?leagueId=25748'
SIMPLE_SIGNIN_ACTION_WITH_REDIRECTION_URL = \
    "http://games.espn.go.com/flb/signin?redir=http%3A%2F%2Fgames.espn.go.com%2Fflb%2Fleaguesetup%2Fownerinfo%3FleagueId%3D25748"
