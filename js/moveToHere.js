// Define movements. This definition can be declared somewhere else. For example in txt, csv, xml file. 
// Before script running movement-data could be converted to JS notation and added to the top of this logic.
//
// 	Sample for definition of parameter move : { table : 'BATTERS', from : 0, to : 'UTIL' }
//
var buttonMoveToHere = function(move) {

	// Preselect table element for optimization and further working with another slot table (Pitchers).
	//
	var tableId = null;

    if ('BATTERS' === move.table.toUpperCase()) {
        tableId = '#playertable_0';
    } else if ('PITCHERS' === move.table.toUpperCase()) {
        tableId = '#playertable_1';
    } else {
        console.error('Wrong table name passed: ' + move.table);
    }

	console.log('selecting table by id: ' + tableId)

	var table = jQuery(tableId) 

	// Select only one [MOVE] button by Index method - eq(from)
	// 
	table.find('tr.pncPlayerRow td.playerEditSlot').eq(move.from).each(function(i, el) {
		console.log('searching MOVE button by position in the table (zero based): ' + move.from)

		// Finding MOVE button and click on it or leave move process.
		// 
		fromBtn = jQuery(this).find('div.pncButtonMove:visible:first')

		if (fromBtn.length) {
			console.log('found MOVE button. Clicking.')

			fromBtn.click();
		} else {
			alert('Player by index: ' + move.from + ' is not available to move. Exiting.');

			return false; // exit from from item moving.
		}

		// sign if player will be moved
		// 
		var movedToHere = false;

        // Select rows by slot name and having HERE button
		//
        var playerSlotsByTo = table.find("tr.pncPlayerRow td.playerSlot")
            // filter by slot name
            .filter(function(){ return jQuery(this).text().toUpperCase() === move.to.toUpperCase(); })
            // filter only having HERE button
            .filter(function(){ return jQuery(this).closest('tr').find('div.pncButtonHere:visible:first').length; });

        console.log('Found slots by [' + move.to + '] active HERE buttons: ' +playerSlotsByTo.length);

        if (playerSlotsByTo.length) {
            // Try to find empty slot, containing - &nbsp; and Click it.
            //
            playerSlotsByTo.each(function(id) {
                console.log('try to find empty slot. ' + id);

                var playerNameElem = jQuery(this).closest('tr').find('td:eq(1)')

                console.log(playerNameElem.text() + ': ' + playerNameElem.text().length)

                if (playerNameElem.html()==='&nbsp;') {
                    console.log('Has empty slot. Click it.');

                    jQuery(this).closest('tr').find('div.pncButtonHere:visible:first').click();

                    movedToHere = true;

                    return false; // Break loop
                }
            });

            if (!movedToHere) {
                console.log('Empty slot was not found. Just click first allowed.');

                playerSlotsByTo.first().closest('tr').find('div.pncButtonHere:visible:first').click();

                movedToHere = true;

                return false; // Break loop
            }
        }
        else {
            console.log('There are not accessible slot with button HERE by slot name: ' + move.to);
        }

		console.log('moved to button [Here] : ' + movedToHere)
		
		// TODO: here may be logic to turn MOVE button back if there not moved to HERE (not found)	

		//alert('moved: ' + move.from + ' to ' + move.to)

		return false;
	});
};
