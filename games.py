# Main script to running Games automation
# Created: 05-May-2014
#
import sys
import csv
import time
import logging
import argparse
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import tools
import signinactions
import settings

# Make movements in UI by JS script.
#
class MoveActionViaJS:

    # Initiate mover
    #
    def __init__(self, driver, screenShot):
        self.driver = driver
        self.screenShot = screenShot

    # Executing script.
    #
    def execute_script(self, script):
        logging.debug(script)
        self.driver.execute_script(script)

    # Release movement
    #
    def move(self, fromToList):

        script = open('js/moveToHere.js').read()

        try:
            # this call by call same script allow UI to make changes before next movement started.
            #
            for move in fromToList:
                funWithArg = "; buttonMoveToHere({{ table : '{}', from: {}, to : '{}' }});".format(move[0], move[1], move[2])
                self.execute_script(script + funWithArg)
                time.sleep(1)
                self.screenShot.create()
        finally:
            logging.debug('BROWSER LOGs:')
            # print messages
            for entry in self.driver.get_log('browser'):
                try:
                    #print entry
                    logging.debug(entry["message"])
                except:
                    logging.warning('Some logs from browser cannot be saved to local log.')


# Main manager class to work with http://games.espn.go.com
#
class Espn:

    # Initiate Espn class by driver and screen shoter.
    #
    def __init__(self, signinAction, driver, screenShot, moveAction):
        self.signinAction = signinAction
        self.driver = driver
        self.screenShot = screenShot
        self.moveAction = moveAction

        self.targetGameEspnUrl=settings.TARGET_MOVEMENT_PATTERN_URL.format(args.leagueid, args.teamid)

        logging.debug('Browser-driver: %s', driver.name)

    # Release Sign in to the site.
    #
    def login(self):
        logging.debug('Start logging to site.')
        self.signinAction.login()
        logging.debug('Logging to site finished.')

    def redirectToTargetUrl(self):
        logging.debug('Transfering to target url.')
        self.get(self.targetGameEspnUrl)
        logging.debug('Finished transfering to target url.')

    # Get page by url
    #
    def get(self, url, forceGet=False):
        if self.driver.current_url != url or forceGet:
            self.driver.get(url)
            logging.debug("Page title is: %s", self.driver.title)
        else:
            logging.debug('Do not redirect, due to same page.')

    # Process movements in UI
    #
    def move(self, fromToList):
        self.moveAction.move(fromToList)

    def submit(self):
        logging.debug('Start submitting movements.')

        save_roster_element = self.driver.find_element_by_id('pncSaveRoster0')
        logging.debug('Button SUBMIT found: %s ', save_roster_element.tag_name)

        self.screenShot.create()

        save_roster_element.click()

        logging.debug('Button SUBMIT clicked')

        time.sleep(1)
        self.screenShot.create()

    def quit(self):
        self.driver.quit()

# Configure logger.
#
def configure_logging():
    logging.basicConfig(
        filename=settings.LOG_FILENAME,
        level=logging.DEBUG,
        format='%(asctime)s [%(filename)s:%(lineno)s] %(levelname)s:%(message)s')


# Get driver by command line parameter in args.driver
#
def getDriver():
        # enable browser logging for getting browser log into this app
        #
        d = DesiredCapabilities.CHROME
        d['loggingPrefs'] = {'browser': 'ALL'}

        # Get driver [Chrome, Firefox, IE, PhantomJS]
        #
        driver = {
          'PhantomJS': lambda: webdriver.PhantomJS(settings.PYTHON_EXECUTE_PATH),
          'Firefox': lambda: webdriver.Firefox(),
          'Chrome': lambda: webdriver.Chrome()
        }[args.driver]()

        # timeout for finding elements by (id, name and etc)
        #
        driver.implicitly_wait(10)

        return driver


# Get Sign In action defined byn args.signinaction
#
def getSinginAction(driver, screenShot, args):

    return {
        'PostForm': lambda: signinactions.SimpleSigninAction(driver, screenShot, args),
        'PostFormWithRedirect': lambda: signinactions.SimpleSigninActionWithRedirecting(driver, screenShot, args),
        'PostAjax': lambda: signinactions.AjaxSigningAction(driver, screenShot, args),
        'PostAjaxViaJS': lambda: signinactions.SimpleSigninActionByJQuery(driver, screenShot, args)
    }[args.signinaction]()


# Main method for process.
#
def start(movingElements):
    try:
        driver = getDriver()

        screenShot = tools.ScreenShot(driver,
                                      namepattern=settings.SCREEN_SHOT_FILENAME_PREFIX,
                                      dirName=settings.SCREEN_SHOT_DIRECTORY)

        # For selecting action should be factory method.
        #
        espn = Espn(getSinginAction(driver, screenShot, args), driver, screenShot, MoveActionViaJS(driver, screenShot))

        espn.login()

        #  Target URL like - http://games.espn.go.com/flb/clubhouse?leagueId=25748&teamId=10&seasonId=2014
        #
        espn.redirectToTargetUrl()

        espn.move(movingElements)

        screenShot.create()
    except:
        logging.exception('Abnormal start.')
    else:
        if args.submit:
            espn.submit()

        if args.quit:
            espn.quit()

# Read movements set from csv file passed in args.file
#
# Example of used CSV file
#       Table,FromPosition,ToSlot
#       Batters,1,UTIL
#       Batters,2,2B/SS
#       ,,
#       pitchers,2,Bench
#
def readMovementsFromCsv():
    logging.debug('Use file: {}'.format(args.file))
    movements = []
    with open(args.file, 'rb') as cf:
        # refine data from header, and non movement rows.
        for crow in csv.reader(cf, delimiter=','):
            if crow[1].isdigit():
                movements.append([crow[0], crow[1], crow[2]])

    return movements

class MyParser(argparse.ArgumentParser):
   def error(self, message):
      sys.stderr.write('\nERROR: %s\n\n' % message)
      self.print_help()
      sys.exit(2)

# Entry point of the Application
#
if __name__ == '__main__':

    configure_logging()

    #parser = argparse.ArgumentParser(description='Make player movement in the tables on site http://games.espn.go.com.')
    parser = MyParser(description='Make player movement in the tables on site http://games.espn.go.com.')

    parser.add_argument('--file', '-file', '--f', '-f', help='filename cvs with movements definition.', required=True)

    parser.add_argument('--driver', '-driver', '--d', '-d',
        help='Browser Driver name to use.', choices=['PhantomJS', 'Firefox', 'Chrome'], default='PhantomJS')

    parser.add_argument('--signinaction', '-signinaction', '-sa', '--sa', help='Sign in action name.',
        choices=['PostForm', 'PostFormWithRedirect', 'PostAjax', 'PostAjaxViaJS'], default='PostForm')

    parser.add_argument('--quit', '-quit', '--q', '-q', help='close browser on the end of the scrip.', action='store_true')
    parser.add_argument('--submit', '-submit', '--s', '-s', help='submit the result of the movements.', action='store_true')

    parser.add_argument('--leagueid', '-leagueid', '--lid', '-lid', help='League Id.', type=int, required=True)
    parser.add_argument('--teamid', '-teamid', '--tid', '-tid', help='Team Id.', type=int, required=True)

    parser.add_argument('--login', '-login', '--l', '-l', help='Login username.', required=True)
    parser.add_argument('--password', '-password', '--p', '-p', help='Login password', required=True)

    args = parser.parse_args()

    print 'Using parameters: ', args

    logging.debug(args)

    start(readMovementsFromCsv())