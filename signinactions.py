# Sign In Action classes.
#
#
import time
import logging
import settings

# Simple authorization without any Ajax. Just by url, input's form feeding and submit.
#
class SimpleSigninAction:

    def __init__(self, driver, screenShot, args):
        self.driver = driver
        self.screenShot = screenShot
        self.args = args

        # http://games.espn.go.com/flb/leaguesetup/ownerinfo?leagueId=25748
        #
        self.simpleSignInUrl = settings.SIMPLE_SIGNIN_ACTION_URL

    def login(self):
        logging.debug('start getting: ' + self.simpleSignInUrl)

        self.driver.get(self.simpleSignInUrl)

        logging.debug('GET called: ' + self.simpleSignInUrl)

        self.screenShot.create()

        # HACK: May be it fix filling form
        #
        logging.debug('sleeping with some secs.')
        time.sleep(2)

        # Find login form
        #
        formElementName = 'loginForm'
        logging.debug('Tring to find loging form element: ' + formElementName)
        form = self.driver.find_element_by_name(formElementName)
        el = form
        logging.debug('Form: %s [is_displayed: %r, is_enabled: %r]', el.tag_name, el.is_displayed(), el.is_enabled())

        # Find and fill login credentals.
        #
        el = form.find_element_by_name('username')
        el.send_keys(self.args.login)
        logging.debug('Input: %s [is_displayed: %r, is_enabled: %r]', el.tag_name, el.is_displayed(), el.is_enabled())

        el = form.find_element_by_name('password')
        el.send_keys(self.args.password)
        logging.debug('Input: %s [is_displayed: %r, is_enabled: %r]', el.tag_name, el.is_displayed(), el.is_enabled())

        self.screenShot.create()
        el = form.find_element_by_name('submit')
        logging.debug('Input-submit: %s [is_displayed: %r, is_enabled: %r]', el.tag_name, el.is_displayed(), el.is_enabled())
        el.click()
        logging.debug('Submitted.')

        self.screenShot.create()

        time.sleep(1)  # Little hack. It' supposed not ready to set auth cookies.


# The as SimpleSigninAction with submit by jQuery script.
#
class SimpleSigninActionByJQuery(SimpleSigninAction):

    def __init__(self, driver, screenShot, args):
        SimpleSigninAction.__init__(self, driver, screenShot, args)

    def login(self):
        logging.debug('start getting: ' + self.simpleSignInUrl)

        self.driver.get(self.simpleSignInUrl)

        logging.debug('GET called: ' + self.simpleSignInUrl)

        self.screenShot.create()

        script = open('js/signin.js').read()

        logging.debug(script)

        # this call by call same script allow UI to make changes before next movement started.
        #
        self.driver.set_script_timeout(30)

        self.driver.execute_script(script)

        time.sleep(5)


# The as SimpleSigninAction, but has redirecting parameter in the URL to move after login
#
class SimpleSigninActionWithRedirecting(SimpleSigninAction):

    def __init__(self, driver, screenShot, args):
        SimpleSigninAction.__init__(self, driver, screenShot, args)

        # http://games.espn.go.com/flb/signin?redir=http%3A%2F%2Fgames.espn.go.com%2Fflb%2Fleaguesetup%2Fownerinfo%3FleagueId%3D25748
        #
        self.simpleSignInUrl = settings.SIMPLE_SIGNIN_ACTION_WITH_REDIRECTION_URL

# Authorization via Ajax popup dialog (iframe). May be not stable.
#
class AjaxSigningAction:

    def __init__(self, driver, screenShot, args):
        self.driver = driver
        self.screenShot = screenShot
        self.args = args

        # http://games.espn.go.com/flb/clubhouse?leagueId=25748&teamId=10&seasonId=2014
        self.pageWithAjaxLoginDialog = settings.AJAX_SIGNING_ACTION_URL

    def login(self):
        logging.debug("Called login via AJAX")

        self.driver.get(self.pageWithAjaxLoginDialog)

        self.screenShot.create()

        # Get Sing-In link
        #
        self.driver.find_element_by_id('personalizationLink').click()

        self.screenShot.create()

        # HACK. Will be changed to appropriate wait for loading iframe
        #
        time.sleep(5)  # HACK

        self.screenShot.create()

        # HACK: selecting iframe should be easy.
        #
        # Getting all iframes. Our with index - 0
        #
        iframes = self.driver.find_elements_by_tag_name('iframe')

        for iframe in iframes:
            self.driver.switch_to_frame(iframe)
            logging.debug('selected iframe: %s', self.driver.current_url)
            break

        self.screenShot.create()

        # Fill loging credentals
        #
        self.driver.find_element_by_id('username').send_keys(self.args.login)
        self.driver.find_element_by_id('password').send_keys(self.args.password)

        self.screenShot.create()
        self.driver.find_element_by_id('submitBtn').click()

        self.screenShot.create()

        # HACK. once again. will be removed.
        #
        time.sleep(5)